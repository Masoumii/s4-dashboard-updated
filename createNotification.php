<html>
    
    <head>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="style.css">
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script async defer src="js/main.js"></script>
    </head>
    
    <body style="background: rgba(0,0,0,0.1);">
        <div id="form-wrapper">
            <form action="notification.controller.php" method="POST">
                <!-- Notificatie content -->
                <input class="notification-content" placeholder="Notificatie tekst" id="content" name="content" type="text" placeholder="Inhoud notificatie" autofocus required><br><br>
                <!-- Notificatie type -->
                <select class="notification-type" id="type" name="type">
                    <option style=" color:#3085d6;" selected="selected" value="info">Info</option>
                    <option style="color:#ffb300;" value="warning">Warning</option>
                    <option style="color:#ff0000;" value="error">Error</option>
                </select>
                <br><br>
                <button name="nCreate" type="submit" class="send-notification" id="">Notificatie verzenden</button>
            </form>
        </div>
    </body>
    
</html>