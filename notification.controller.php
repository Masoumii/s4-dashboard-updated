<?php

/* Get notifications */
if (isset($_GET['nGet'])){
    require 'notification.class.php';
    $notification = new Notification();
    $notification->getNotification();
}


/* Mark notification as read */
if (isset($_POST['nRead'])){
    require 'notification.class.php';
    $notification = new Notification();
    $notification->readNotification();
}  


/* Create notifications */
if(isset($_POST['nCreate'])){
    
    $content = $_POST['content'];
    $type    = $_POST['type'];  

    require 'notification.class.php';
    $notification = new Notification();
    $notification->createNotification($content, $type);
    header("location: createNotification.php ");
}