$(function(){

    /* App Instellingen */
    const FORECAST_MAIN_URL = "https://api.openweathermap.org/data/2.5/forecast?q=";
    const FORECAST_LOCATION = "Laren,Nederland";
    const FORECAST_UNITS    = "Metric";
    const FORECAST_API_KEY  = "ac953f185f3e30fdf061e93ab48e2bc9";
    const FORECAST_IMG_URL  = "https://openweathermap.org/img/w/";
    const FULL_URL          = FORECAST_MAIN_URL + FORECAST_LOCATION + "&units=" + FORECAST_UNITS + "&appid=" + FORECAST_API_KEY;
    
    /* Haal voorspelling op */
    let forecast = $.getJSON( FULL_URL, function(){});

    /* Als het ophalen van de voorspelling gelukt is */
    forecast.done(function(data){

        /* Iedere lijst (dag) ophalen */
        $.each(data.list, function(mainIndex, mainValue){ 

            /* Datum / tijd van voorspelling */
            let forecastTime = mainValue.dt_txt;
            
            /* Alleen middag data (15:00u) ophalen */
            if(forecastTime.includes("15:00:00")){

                let forecastDate = new Date(mainValue.dt * 1000);                       // Converteer naar miliseconden
                let days         = ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Zat'];         // Alle mogelijke dagen in een week
                
                let dayName      = days[forecastDate.getDay()];                         // Bepaalt dagen a.d.h.v. opgehaalde datum (forecastDate)
                let forecastTemp = Math.round(mainValue.main.temp)+"℃";                // Temperatuur
                let forecastIcon = FORECAST_IMG_URL + mainValue.weather[0].icon+".png"; // Icoontjes weersomstandigheden

                /* Maak elementen aan */
                let weatherItem  = $("<div class='weatherItem'></div>");
                let weatherDay   = $("<span class='weatherDay'></span><br>");
                let weatherImage = $("<img class='weatherImage' src=''><br>");
                let weatherTemp  = $("<span class='weatherTemp'></span>");
                
                /* Elementen op pagina plaatsen/toevoegen */
                weatherTemp.html(forecastTemp);
                weatherDay.html(dayName);
                weatherImage.attr("src", forecastIcon);
                weatherItem.append(weatherDay).append(weatherImage).append(weatherTemp);
                $("#weatherItems").append(weatherItem);

            };
        });
    });
    
    /* Als voorspelling ophalen mislukt is */
    forecast.fail(function(){
        alert("Voorspelling van het weer kon niet worden geladen!");
    });
    
});
