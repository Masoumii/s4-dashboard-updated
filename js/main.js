

/* GLOBALE VARIABELEN */
let currentStep       = 1;	  // Houdt de huidige stap / pagina bij
let minStep           = 1;	  // Houdt bij wat de eerste stap/pagina is
let timedPageSwitch   = null;     // Initial Interval var (leeg)

/* APP INSTELLINGEN */
const INTERVAL_SECONDS = 20000;   // Om de zoveel miniseconden van pagina veranderen			
const MAX_STEP         = 4;	  // Houdt bij hoevaal stappen / pagina's er maximaal zijn ( Verhogen als er een nieuwe pagina erbij komt )

/* Navigatie-buttons - Vorige & Volgende */
const nextStep        = $('#nextStep');
const previousStep    = $('#previousStep');

/* Navigatie-buttons - Eerste & Laatste */
const fastForward     = $('#fastForward');
const fastBackward    = $('#fastBackward');

/* Checkt wat de huidige stap en bepaalt of knoppen disabled moeten worden of niet */
function checkCurrentStep(currentStep) {
	
	// Als eerste stap/pagina actief is, deactiveer buttons "vorige" en "eerste"
	if (currentStep === minStep) { 
		previousStep.css("opacity","0.5");
		fastBackward.css("opacity","0.5");
	}else{
		previousStep.css("opacity","1");
		fastBackward.css("opacity","1");
	}

	// Als laatste stap/pagina actief is, deactiveer buttons "volgende" en "laatste"
	if (currentStep === MAX_STEP) { 
		nextStep.css("opacity","0.5");
		fastForward.css("opacity","0.5");
	}else{
		nextStep.css("opacity","1");
		fastForward.css("opacity","1");
	}
}

/* Generieke functie om request te maken om vorige/volgende pagina's op te halen a.d.h.v. "currentStep" */
function requestStep(currentStep) {

	/* Haal de pagina op */
	let getStep = $.get("step" + currentStep + ".html", function(data){});

	/* Als AJAX-Request gelukt is */
	getStep.done(function(data){
	    checkCurrentStep(currentStep);
		$('.container').html(data).hide().fadeIn(1000);
	});

	/* Als AJAX-Request niet gelukt is */
	getStep.fail(function(){
		$('.container').html("Failed");
	});
}

/* Initial page-load */
$(function(){
	checkCurrentStep(1); // Kijkt wat de huidige stap is (Om te bepalen wat de vorige/volgende wordt)	
	requestStep(1);      // Haalt de pagina op
});

/* Functie: Page switch om de x seconden */
function switchPageInterval( condition ){

	// (Re)start de interval
	if( condition === true ){

		 timedPageSwitch = setInterval( function() {
			// Als laatste stap bereikt is, loop weer vanaf het begin ( Eerste pagina )
			if (currentStep === minStep){
				previousStep.css("visibility","1");
				fastBackward.css("opacity","1");
			}
			if (currentStep === MAX_STEP){
				previousStep.css("opacity","0.5");
				fastBackward.css("opacity","0.5");
			}
			// Switch page: Ga naar volgende pagina
			nextView();

		  }, INTERVAL_SECONDS);
	}

	// Stop de interval
	else if( condition === false ){
		timedPageSwitch = clearInterval(timedPageSwitch);

		if(!timedPageSwitch){
			$(".resetInterval").css("visibility","hidden");
			
		}else{
			$(".resetInterval").css("visibility","visible");
		}
		console.log("De slideshow is gestopt");
	}
	
	// Geen argumenten meegegeven tijdens initialiseren van de functie
	else{
		console.log( "No arguments given for function: switchPageInterval();" );
	}
}

/* Voer de functie uit als de pagina geladen is */
switchPageInterval(true ,3000);

/* Stop de page-switch interval als er geklikt wordt op de Navigatie-buttons */
$(".goToNext, .goToPrevious, .goToFirst, .goToLast").on("click", function(){
	switchPageInterval(false);
	$(".resetInterval").css("visibility","visible");
});

/* Volgende stap functie */
function nextView() {

	if (currentStep !== MAX_STEP) { // Als de app niet op de laatste pagina zit
		currentStep += 1;		  // Ga naar volgende stap/pagina
		requestStep(currentStep);
		//console.log("Huidige pagina: " + currentStep);
	}else{
		currentStep = 1;		 
		requestStep(currentStep);
		//console.log("Huidige pagina: " + currentStep);
	}

	/* Schakel bepaalde navigatie-buttons uit a.d.h.v. huidige pagina */
	if(currentStep === MAX_STEP){ // Verberg Next en Last buttons als laatste pagina actief is
		nextStep.css("opacity","0.5");
		fastForward.css("opacity","0.5");
	}
	if(currentStep === minStep){ // Verberg Previous en First buttons als laatste pagina actief is
		previousStep.css("opacity","0.5");
		fastBackward.css("opacity","0.5");
	}
        
}

/* Vorige stap functie */
function previousView() {
	
	if (currentStep !== minStep) { // Als de app niet op eerste pagina zit
		currentStep -= 1;		  // Ga naar vorige stap/pagina
		requestStep(currentStep);
		//console.log("Huidige pagina: " + currentStep);
	}
}

/* Fast forward- and backward button functies ( Eerste & Laatste ) */
fastForward.on("click", function () {
	checkCurrentStep(); 
	currentStep = MAX_STEP;
	requestStep(currentStep);
});
fastBackward.on("click", function () {
	checkCurrentStep();
	currentStep = minStep;
	requestStep(currentStep);
});

/* Restart de interval on-click */
$(".resetInterval").on("click", function () {

	// Als de interval uitgeschakeld is, schakel hem dan in.
	if(!timedPageSwitch){
		$(".resetInterval").css("visibility","hidden");
		switchPageInterval(true, 3000);
		console.log("De slideshow is weer gestart!");
	// If the interval is turned on already, show it 
	}else{
		$(".resetInterval").css("visibility","visible");
	}

});

/* Hover-Effects voor de navigatiebuttons */
$(".goToNext, .goToPrevious, .goToFirst, .goToLast, .resetInterval").hover(function(){
	$(this).addClass("btnHover");
}).mouseout(function(){
	$(this).removeClass("btnHover");
});


/* Notificatie verzenden */
$("#submit").on("click", function(){

    /* Formulier variabelen */
    let notifyContent = $(".notification-content").val(); // Waarde vakje content notificatie
    let notifyType    = $(".notification-type").val();    // Waarde vakje type notificatie
    
    /* Als notificatie content niet leeg is */
    if(notifyContent !== ""){
        
        /* POST de notificatie */
        let sendNotification = $.post( "createNotification.php", {
            content:notifyContent, // De content van de notificatie
            type:notifyType        // De type van de notificatie
        }); 
      
        /* Als POST notificatie gelukt is */
        sendNotification.done(function(){
            /* Vertraag verzenden van de notificatie */
           // $("#submit").fadeOut().delay(12000).fadeIn();
        });
        
        /* Als POST notificatie mislukt is */
        sendNotification.fail(function(){
           alert("Notificatie verzenden is mislukt"); 
        });
      
    }else{
        alert("Er moet een titel ingevoerd zijn");
    }
});