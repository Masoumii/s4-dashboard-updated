$(function(){
    
    /* App Instellingen */
    const NO_ACTIVITY_DAYS_AGO    = 30;
    const NO_ACTIVITY_URL         = "https://s4financials.s4analysis.nl/last-activity/noactivitylist?days="; // URL om JSON eruit te halen
    const NO_ACTIVITY_TIMEFRAME   = 60; // Laatste activiteit klanten in de afgelopen x minuten
    const NO_ACTIVITY_INTERVAL_MS = 60000; // Haal data online gebruikers op om de x miliseconden    
    
    /* Main Functie */
    function getAllOfflineUsers(){
        
        /* Globale variabelen */
        let offlineUserCustomer          = null; // Naam klant
        let offlineLastActivityTimestamp = [];   // Tijdstip/datum laatste activiteit klant
        let offlineUniqueNames           = [];   // Array met alleen unieke klantnamen
        let offlineUniqueOfflineNames    = [];   // Array met alleen unieke klantnamen die niet actief zijn geweest
        let offlineLastActivityArray     = [];   // Array met online gebruikers
        let offlineNoActivityArray       = [];   // Array met gebruikers die een tijdje niet zijn ingelogd
        let offlineCountNames            =  0;   // Variabele aantal online gebruikers
        
        /* Haal alle online gebruikers op */
        let getOfflineUsers = $.getJSON(NO_ACTIVITY_URL + NO_ACTIVITY_DAYS_AGO, function(){});

        /* Als het ophalen van online gebruikers gelukt is */
        getOfflineUsers.done(function(response){

            /* Success: True of False */
             let success = response.success;

            /* Run de applicatie enkel als success op true staat */
             if(success === true){

            /* Loop om alle klantnamen van de json met offline gebruikers eruit te halen */
            $.each(response.data, function(offlineIndex, offlineValue){

                    /* Algemene klantnaam (dus geen "username") */
                    offlineUserCustomer = offlineValue.customer;                    
                    offlineLastActivityArray.push(offlineUserCustomer); // Zet klantnaam in array  
                         
                        /* Haal alleen unieke klantnamen uit de array en stop de unieke klantnamen in "uniqueNames" array hierboven*/
                        $.each(offlineLastActivityArray, function(i, customerName){ 
                            if($.inArray(customerName, offlineUniqueNames) === -1){
                            offlineUniqueNames.push(customerName); // Zet alleen unieke klantnamen in array
                                }
                            });                                                                                                 
            });

                /* Sorteer op alfabetische volgorde */
                offlineUniqueNames.sort();

                /* Houdt bij: alle offline gebruikers in de array met unieke offline klantnamen */
                if(offlineUniqueNames.length >=1){
                offlineCountNames = offlineUniqueNames.length;
                //console.log(countNames);
                }else{
                    offlineCountNames = 0;
                    //$(".countUsers").css("display","none");
                }               

                /* Empty the div before appending new data */
                 $("#NoActivityUsers-content").html("");
                 
                 //$(".afgelopen").html("(In de afgelopen "+NO_ACTIVITY_TIMEFRAME+" min)");

                /* (Unieke) online klanten weergeven */
                $.each(offlineUniqueNames, function(offlineNamesIndex, offlineNamesValue){
                    $(".countOfflineUsers").html("<b>"+offlineCountNames+"</b>");
                    $("#NoActivityUsers-content").append("<div class='offlineUsersItem'>"+offlineNamesValue+"</div><br>");
                });
             }
        });

        /* Als het ophalen van online gebruikers NIET gelukt is */
        getOfflineUsers.fail(function(){
           console.error("Lijst met no-activity gebruikers kan niet worden opgehaald"); 
        });
    }

    /* Run de main functie bij eerste keer page-load */
    getAllOfflineUsers();
    
    /* Start vervolgens de interval om de main functie om de x(INTERVAL_MS) seconden uit te voeren */
    setInterval(getAllOfflineUsers, NO_ACTIVITY_INTERVAL_MS);
    
});