$(function(){
    
    /* globale variabelen */
    let notificationContent   = null;
    let notificationTimestamp = null;
    let notificationStatus    = null;
    let notificationType      = null;
    let notificationColor     = null;
    let notificationIcon      = null;
      
    /* Instellingen */
    const NOTIFICATION_SOUND  = new Audio("resources/audio/notification.mp3"); // Notificatie geluid
    const NOTIFICATION_DELAY  = 10000; // Aantal milliseconden dat een notificatie zichtbaar is
    const INTERVAL_SECONDS    = 1000;
    
    /* Bronnen afbeeldingen notificatie */
    const INFO_ICON            = "resources/images/info.png";    // Type info (default)
    const WARNING_ICON         = "resources/images/warning.png"; // Type warning
    const ERROR_ICON           = "resources/images/error.png";   // Type error
    
    /* Kleuren notificatiescherm , Wordt bepaald a.d.h.v. type notificatie  */
    const BLUE   = "#3085d6";
    const ORANGE = "#f7b600";
    const RED    = "#ff0000";

        /* Main functie: Haal notificatie(s) op */
        function getNotifications(){
        
            /* JSON ophalen */
            let getNotifications = $.getJSON("notification.controller.php", {nGet:true}, function(){});

            /* Als JSON ophalen gelukt is */
             getNotifications.done(function(data){
                 
                 /* Als data niet leeg is */
                 if (!jQuery.isEmptyObject(data)){

                       /* JSON data assignen aan variabelen */
                        notificationContent   = data.content;   // Content van notificatie
                        notificationTimestamp = data.timestamp; // Timestamp van notificatie
                        notificationStatus    = data.status;    // Status van notificatie ( 0 of 1, gelezen of ongelezen )
                        notificationType      = data.type;      // Type van notificatie ( Info, Warning of Error )
                        
                        /* Bepaal notificatiescherm achtergondkleur en icoon a.d.h.v. type notificatie */
                        /* INFO */
                        if(notificationType === "info"){
                            notificationColor = BLUE;
                            notificationIcon  = INFO_ICON;
                        }
                        /* WARNING */
                        if(notificationType === "warning"){
                            notificationColor = ORANGE;
                            notificationIcon = WARNING_ICON;
                        }
                        /* ERROR */
                        if(notificationType === "error"){
                            notificationColor = RED;
                            notificationIcon  = ERROR_ICON;
                        }
                                 
                        /* Slide-down, geef notificatie content en type(achtergrondkleur) mee, wacht x seconden en slide weer naar boven */
                        $("#notifications")
                        .css("background", notificationColor)
                        .slideDown(1000)
                        .html("<img class='warning-icon-left' src='"+notificationIcon+"'>"+notificationContent+" ("+notificationTimestamp+")<img class='warning-icon-right' src='"+notificationIcon+"'>")
                        .delay(NOTIFICATION_DELAY)
                        .slideUp(1000);
                
                        /* Speel een meldingsgeluid af bij een nieuwe notificatie */
                        NOTIFICATION_SOUND.play();
                    
                        /* Debugging: Check of gegevens worden geladen  */
                        console.log("JSON gegevens zijn succesvol opgehaald: "+JSON.stringify(data));

                        /* Markeer notificatie als gelezen in de database zodra de notificatie getoond wordt */
                        let markAsRead = $.post("notification.controller.php",{nRead:true}, function(data){});

                        /* Als POST REQUEST gelukt is */
                        markAsRead.done(function(){
                           console.log("Notificatie gemarkeerd als gelezen!"); 
                        });

                        /* Als POST REQUEST niet gelukt is */
                        markAsRead.fail(function(){
                            console.error("Notificatie markeren als gelezen niet gelukt!");
                        });
                    }
            });
            
            /* Als JSON ophalen niet gelukt is */
            getNotifications.fail(function( ){
                    console.error("Fout: Niet gelukt om de JSON op te halen of te verbinden met de database");
            });
        }
        
        /* Initialiseer de interval om notificaties op te halen */
        setInterval(getNotifications, INTERVAL_SECONDS);
        
});