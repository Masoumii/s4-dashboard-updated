$(function(){
    
    /* App Instellingen */
    const LAST_ACTIVITY_URL       = "https://s4financials.s4analysis.nl/last-activity/list"; // URL om JSON eruit te halen
    const LAST_ACTIVITY_TIMEFRAME = 60; // Laatste activiteit klanten in de afgelopen x minuten
    const INTERVAL_MS             = 60000; // Haal data online gebruikers op om de x miliseconden    
    
    /* Main Functie */
    function getAllOnlineUsers(){
        
        /* Globale variabelen */
        let userCustomer          = null; // Naam klant
        let lastActivityTimestamp = [];   // Tijdstip/datum laatste activiteit klant
        let uniqueNames           = [];   // Array met alleen unieke klantnamen
        let uniqueOfflineNames    = [];
        let lastActivityArray     = [];   // Array met online gebruikers
        let noActivityArray       = [];   // Array met gebruikers die een tijdje niet zijn ingelogd
        let countNames            =  0;   // Variabele aantal online gebruikers
        //let minutesAgo          =  0;   // ( LAST ACTIVITY MINUTES AGO )
        
        /*Vandaag */
        let datetimeVandaag = new Date(); // Timestamp
//        let lastWeek = new Date(); // Vandaag, voor de omzetting hieronder
//        lastWeek.setDate (lastWeek.getDate()-7); // Vorige week berekenen
        
        /* In het verleden */
        let datetimeInPast  = new Date(datetimeVandaag.getTime() - LAST_ACTIVITY_TIMEFRAME * 60000); // Tijdstip ( x min geleden )
        
        /* Haal alle online gebruikers op */
        let getUsers = $.getJSON(LAST_ACTIVITY_URL, function(){});

        /* Als het ophalen van online gebruikers gelukt is */
        getUsers.done(function(response){

            /* Success: True of False */
             let success = response.success;

            /* Run de applicatie enkel als success op true staat */
             if(success === true){

            /* Loop om alle klantnamen van de json met online gebruikers eruit te halen */
            $.each(response.data, function(mainIndex, mainValue){

                    /* Algemene klantnaam (dus geen "username") */
                    userCustomer = mainValue.customer; 
                    /* Laatste activiteit */
                    lastActivityTimestamp = new Date(mainValue.last_activity_time); // Timestamp
                    
                    /* Als klant binnen de afgelopen x(datetimeInPast) minuten geleden actief is geweest */
                    if(lastActivityTimestamp > datetimeInPast){
                        
                          /* LAST ACTIVITY MINUTES AGO ) */
//                        /* Wordt gebruikt voor object hierbeneden ( Berekening hoelang geleden gebruiker actief is geweest ) */
//                        let startDate = new Date(lastActivityTimestamp);
//                        let endDate   = new Date();
//                        let seconds   = Math.round((endDate.getTime() - startDate.getTime()) / 1000);
//                        minutesAgo   = Math.round(seconds / 60);
                         
                        lastActivityArray.push(userCustomer); // Zet klantnaam in array  
                         
                        /* Haal alleen unieke klantnamen uit de array en stop de unieke klantnamen in "uniqueNames" array hierboven*/
                        $.each(lastActivityArray, function(i, customerName){ 
                            if($.inArray(customerName, uniqueNames) === -1){
                            uniqueNames.push(customerName); // Zet alleen unieke klantnamen in array
                                }
                            });                                          
                    }
                    
                    
//                    if( lastActivityTimestamp < datetimeInPast && lastActivityTimestamp > lastWeek ){
//                        
//                        noActivityArray.push(userCustomer);
//                        
//                         $.each(noActivityArray, function(key, customerName){ 
//                            if($.inArray(customerName, uniqueOfflineNames) === -1){
//                            uniqueOfflineNames.push(customerName); // Zet alleen unieke klantnamen in array
//                                }
//                            });  
//                            
//                            console.log(uniqueOfflineNames);
//                    }
                                                           
            });

                /* Sorteer op alfabetische volgorde */
                uniqueNames.sort();

                /* Houdt bij: alle online gebruikers in de array met unieke klantnamen */
                if(uniqueNames.length >=1){
                countNames = uniqueNames.length;
                }else{
                    countNames = 0;
                    $(".countUsers").css("display","none");
                }               

                /* Empty the div before appending new data */
                 $("#widget2-content").html("");
                 
                 $(".afgelopen").html("( In de afgelopen "+LAST_ACTIVITY_TIMEFRAME+" min )");

                /* (Unieke) online klanten weergeven */
                $.each(uniqueNames, function(namesIndex, namesValue){
                    $(".countUsers").html("<b>"+countNames+"</b>");
                    $("#widget2-content").append("<div class='onlineUsersItem'>"+namesValue+"</div><br>");
                });
             }
        });

        /* Als het ophalen van online gebruikers NIET gelukt is */
        getUsers.fail(function(){
           console.error("Lijst met online gebruikers kan niet worden opgehaald"); 
           //alert("Lijst met online gebruikers kan niet worden opgehaald");
        });
    }

    /* Run de main functie bij eerste keer page-load */
    getAllOnlineUsers();
    
    /* Start vervolgens de interval om de main functie om de x(INTERVAL_MS) seconden uit te voeren */
    setInterval(getAllOnlineUsers, INTERVAL_MS);
    //let restartInterval = reflow;
    
});