$(function(){
    
    /* App Instellingen */
    const NEWS_MAIN_URL = "https://newsapi.org/v2/top-headlines?country=";
    const NEWS_COUNTRY  = "nl";
    const NEWS_API_KEY  = "2c84eb61c300405d9c8c110539be9be9";
    const NEWS_FULL_URL = NEWS_MAIN_URL + NEWS_COUNTRY + "&apiKey=" + NEWS_API_KEY;
    const NEWS_REFRESH_RATE = 1800000;  // Om de 30 min nieuwsberichten update/ophalen
           
           
           /* Main functie om nieuwsberichten op te halen */
           function getNewsMain(){
    /* Haal nieuwsberichten op uit API */
    let getNews = $.get(NEWS_FULL_URL, function(){});
            
    /* Als het ophalen gelukt is */
    getNews.done(function(data){

        $.each(data.articles, function(index, value){
                   
            /* Alleen 8 artikelen ophalen voor de 8 grids op de pagina */
            if (index > 7){
            return false;
            }

            /* Nieuws waarden uit JSON */
            let newsTitle       = value.title;       // Titel artikel
            let newsSource      = value.source.name; // Naam bron artikel
            let newsDate        = value.publishedAt; // server time format
            let newsImage       = value.urlToImage;  // URL naar afbeelding artikel
            let newsURL         = value.url;         // URL naar het artikel zelf

            /* Formateer datum */
            let stringifiedDate = new Date(newsDate).toISOString(newsDate);
            let parsedDate      = Date.parse(stringifiedDate); 
            let cardDate        = new Date(parsedDate); 
            let finalDate       = new Date(cardDate).toLocaleString("nl-NL");
                    
            /* Placeholder afbeelding als er geen artikel afbeelding is */
            if(newsImage === null){
                newsImage = "resources/images/news-placeholder.png";
            }
            
            if(newsSource === "Telegraaf.nl"){
                 newsImage = "resources/images/news-placeholder.png";
            }
           
            /* Nieuws-grid & links aanmaken/aanwijzen */
            let newsContainer = $("<div style='background:url("+newsImage+");' class='newsItem'></div>");
            let newsLink      = $("<a class='newsItem-title' target='_blank' href='"+newsURL+"' >"+newsTitle+"</a>");
                    
            /* Nieuws-grid en links vullen */
            newsLink.html(newsTitle);       // Zet title in de span
            newsContainer.append(newsLink); // Zet title in de container   
            $("#newsItems").append(newsContainer); // Zet de container op de pagina neer

        });
    });
            
    /* Als het ophalen mislukt is */
    getNews.fail(function(){
        console.error("Niet gelukt om het nieuws op te halen");
    });
    
           }
           
           getNewsMain();
           
           setInterval(getNewsMain, NEWS_REFRESH_RATE);
           
           
});