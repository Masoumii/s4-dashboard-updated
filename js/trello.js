$(function() {

    /* APP INSTELLINGEN */
    const API_MAIN    = "https://api.trello.com/1/board/"; // Main URL van de trello API
    const SHORTCODE   = "hPw0kl6W"; // Shortcode van de trello-bord  (bijv: https://trello.com/b/hPw0kl6W/ )
    const API_KEY     = "a0ef4de986e403ea8826eff2f0edab3e"; // Trello API Key, te verkrijgen op https://trello.com/app-key
    const API_TOKEN   = "a4d151c42190354506c3ec07f13b8f59701f6124c3b6a0459bbc1b889ec91bf7"; // Trello API Token, Ook te verkrijgen op https://trello.com/app-key
    const CARD_STATUS = "open"; // "open/visible" of "closed" ( Open of gesloten kaarten ophalen )

    /* initiële waarde bordnaam (leeg) totdat data uit Trello wordt opgehaald */
    let boardName     = null;
    let cardName      = null;
    let dateString    = null;
    let dueCards      = {};
    let cardUrl       = null;
    let dateColor     = null;
    let cardDate      = null;
    let myArray =     [];

    /* Haal JSON data op uit TRELLO API */
    let trelloData    = $.getJSON( API_MAIN + SHORTCODE + "?&key=" + API_KEY + "&token=" + API_TOKEN + "&cards=" + CARD_STATUS, function(){});

        /* Als JSON succesvol opgehaald is */
        trelloData.done(function(data) {

            /* Pas algemene bordnaam toe */
            boardName = data.name;

            /* Voor iedere kaart, Haal alle gegevens uit de JSON */
            $.each( data.cards, function(key, value ) {
                
                /* Kaartgegevens */
                cardName = value.name;     // Naam van trello-kaart
                cardDate = value.due;      // vervaldatum van trello-kaart
                cardUrl  = value.shortUrl; // URL van kaart
               
                /* Datums */
                let today    = new Date(); // vandaag
                let nextWeek = new Date(today.getTime() + 7 * 24 * 60 * 60 * 1000); // Precies een week later
             
                
                /* Formateer datum van alleen kaarten met een vervaldatum */
                
                     if (cardDate !== null) { // Als er een vervaldatum is
                     let dueDate = new Date(cardDate).toISOString(cardDate); // Vervaldatum kaart 
                     let parsedDate = Date.parse(dueDate); // Parse datum
                     cardDate       = new Date(parsedDate); // Return de datum
                    

                    /* Alleen kaarten met een vervaldatum in de toekomst (inclusief vandaag) die binnen maximaal een week te komen vervallen */
                    if ( cardDate >= today && cardDate <= nextWeek ) {
                                  
                        /* Bereken hoeveel dagen er nog over zijn tot deadline */
                        let daysLeft  = Math.round((cardDate - today) / (1000 * 60 * 60 * 24));

                        /* Maak object aan met kaartnaam en vervaldatum van kaart */
                        dueCards = {
                            "name":[cardName]
                        };

                        /* Formateer next week naar leesbare NL datum */
                        let formattedWeek = nextWeek.toLocaleDateString("nl-NL");

                        /* Vul object in met naam, datum en deadline datum */
                       $.each(dueCards, function(){
                           
                                               myArray.push({
                                                name:     cardName,
                                                date:     parsedDate,
                                                url:      cardUrl,
                                                daysLeft: daysLeft
                                            }); 

                                           myArray.sort(function(a, b) { 
                                               return (a.date - b.date) || a.name.localeCompare(b.name); 
                                           });                             

                       });
                       
                        /* Bereken en toon deadline datum */
                        $(".container .trello-board").html("Deadline tot volgende week (" + formattedWeek + ")");
                    
                    }
                }
            });
          
            /* Verander de titel van Trello-pagina naar naam opgehaalde trello-bord */
            $(".trello-title").text(boardName);
            
             $.each( myArray, function(rowKey, rowValue ) {
                 
                
                        
                 let finalCardName = rowValue.name;
                 let finalCardDate = rowValue.date;
                 let finalCardUrl  = rowValue.url;
                 let finalDaysLeft = rowValue.daysLeft;
                 let formatDateOne = new Date(finalCardDate); // Return de datum
                 let formatDateTwo = new Date(formatDateOne).toLocaleDateString("nl-NL"); 
                 
                   /* Bepaal kleur datum a.d.h.v. aantal dagen over tot deadline */
                        if (finalDaysLeft <= 3) {
                            dateColor = "#ff0000"; // Rood
                        }
                        if (finalDaysLeft > 3 && finalDaysLeft <= 5) {
                            dateColor = "#ffb300"; // Oranje
                        }
                        if (finalDaysLeft > 5) {
                            dateColor = "#5aafff"; // Oranje
                        }
                        
                        
 
                 if(finalDaysLeft === 1){
                    dateString = "dag";
                 }
                 if(finalDaysLeft > 1){
                     dateString = "dagen";
                 }
                 
                 if(finalDaysLeft === 0){
                     finalDaysLeft = "";
                     dateString = "DEADLINE VERLOPEN";
                 }

                 $(".container #trello").append("<span class='trelloRow'><a target='_blank' href='"+finalCardUrl+"'>"+finalCardName + " <span style='color:" + dateColor + "' class='formattedDate'>" + formatDateTwo + "</span>&nbsp;<span style='color:" + dateColor + "' class='formattedDate'>" + finalDaysLeft +" "+ dateString+"</span></span><br><br");
             });
            
        });

        /* Als het ophalen van JSON niet gelukt is */
        trelloData.fail(function() {
            console.error("Niet gelukt om de JSON op te halen \n of Trello API is momenteel niet beschikbaar");
        });
});