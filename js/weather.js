$(function() {

    /* APP Instellingen */
    const API_MAIN           = "https://api.openweathermap.org/data/2.5/weather?q="; // Main URL voor de OpenWeatherMap API
    const API_ICON_SRC       = "https://openweathermap.org/img/w/";                   // Main URL om icoon huidige weersomstandigheden op te halen
    const API_KEY            = "ac953f185f3e30fdf061e93ab48e2bc9";                   // OpenWeatherMap API sleutel
    const UNITS              = "metric";                                             // metric(Celcius) of imperial(Fahrenheit)
    const CITY_TO_QUERY      = "Laren, Nederland";                                   // Gekozen stad:  "Laren" of "Laren, Nederland"
    const REFRESH_RATE       = 120000;                                               // ( Iedere 2 minuten gegevens updaten )
    
    /* Bronnen Icoontjes */                                         
    const TEMP_IMG           = "resources/images/temp.svg";                          // Temperatuur 
    const HUMIDITY_IMG       = "resources/images/humidity.svg";                      // Lucht vochtigheid
    const SUNRISE_IMG        = "resources/images/sunrise.svg";                       // Zonsopgang 
    const SUNSET_IMG         = "resources/images/sunset.svg";                        // Zonsondergang
    const WIND_IMG           = "resources/images/wind.svg";                          // Wind
    const WIND_DEG_IMG       = "resources/images/windDirection2.svg";                // Wind richting
    
    /* Bronnen Video's */
    const VIDEO_RAINING      = "raining.mp4";                                        // Regen
    const VIDEO_SUNNY        = "sunny.mp4";                                          // Zonnig
    const VIDEO_STORM        = "storm.mp4";                                          // Storm
    const VIDEO_SNOW         = "snowing.mp4";                                        // Sneeuw
    const VIDEO_FOGGY        = "foggy.mp4";                                          // Mist
    const VIDEO_CLOUDY       = "cloudy.mp4";                                         // Bewolkt
    const VIDEO_LIGHT_CLOUDY = "light-cloudy.mp4";                                   // Licht bewolkt
    const VIDEO_SAND         = "sandstorm.mp4";                                      // Zandstorm


    /* MAIN Functie */
    function getWeather() {

        /* Haal weer-data op vanuit de API */
        $.get( API_MAIN + CITY_TO_QUERY + "&units=" + UNITS + "&appid=" + API_KEY, function(data) {

            /* Weer Object */
            let weatherMain  = data.weather[0].main;             // Hoofd beschrijving van het huidige weersomstandigheden
            let weatherIcon  = data.weather[0].icon;             // Icoon voor huidige weersomstandigheden

            /* Main Object */
            let mainTemp     = Math.round(data.main.temp);       // Huidige temperatuur
            let mainHumidity = data.main.humidity;               // Huidige luchtvochtigheid

            /* Wind Object */
            let windSpeed    = Math.round(data.wind.speed * 3.6); // Snelheid wind
            let windDegDesc  = "Onbekend";                        // Standaard beschrijving wind richting ( Als JSON veld leeg is )
            let windDeg      = data.wind.deg;                     // Wind richting (graden)
            
            /* Windrichting beschrijving bepalen a.d.h.v. graden richting ( 1 t/m 360 graden ) */
            if(windDeg >= 0 && windDeg < 90){
                windDegDesc = "Noordoosten";
            }
            if(windDeg === 90){
                windDegDesc = "Oosten";
            }
            if(windDeg > 90 && windDeg < 180){
                windDegDesc = "Zuidoosten";
            }
            if(windDeg === 180){
                windDegDesc = "Zuiden";
            }
            if(windDeg > 180 && windDeg < 270){
                windDegDesc = "Zuidwesten";
            }
            if(windDeg === 270){
                windDegDesc = "Westen";
            }
            if(windDeg > 270 && windDeg < 360){
                windDegDesc = "Noordoosten";
            }
            if(windDeg === 0 || windDeg === 360){
                windDegDesc = "Noorden";
            }
            
            /* Zonsopgang- & ondergang */
            let systemSunriseHour = new Date(data.sys.sunrise * 1000).getHours();
            let systemSunriseMin  = new Date(data.sys.sunrise * 1000).getMinutes();
            let systemSunsetHour  = new Date(data.sys.sunset  * 1000).getHours();
            let systemSunsetMin   = new Date(data.sys.sunset  * 1000).getMinutes();

            /* Formateer sunrise minuten */
            function addLeadingZeroSunrise(minute) {
                if (minute < 10) {
                    systemSunriseMin = "0" + minute;
                } else {
                    systemSunriseMin = minute;
                }
                return systemSunriseMin;
            }

            /* Formateer sunset minuten */
            function addLeadingZeroSunset(minute) {
                if (minute < 10) {
                    systemSunsetMin = "0" + minute;
                } else {
                    systemSunsetMin = minute;
                }
                return systemSunsetMin;
            }

            /* Formateer sunrise en sunset minuten en return de values */
            addLeadingZeroSunrise(systemSunriseMin);
            addLeadingZeroSunset(systemSunsetMin);

            /* Combineer uren en minuten en assign ze aan variabelen */
            let systemSunrise    = systemSunriseHour + ":" + systemSunriseMin;
            let systemSunset     = systemSunsetHour  + ":" + systemSunsetMin;

            /* Bepaalt wat de achtergrond-video wordt a.d.h.v. huidige weersomstandigheden */
            let backgroundVideoSrc = null;
            
            switch (weatherMain) {               
               
                case "Clear":
                    backgroundVideoSrc = VIDEO_SUNNY;
                    break;

                case "Rain":
                    backgroundVideoSrc = VIDEO_RAINING;
                    break;

                case "Drizzle":
                    backgroundVideoSrc = VIDEO_RAINING;
                    break;

                case "Storm":
                    backgroundVideoSrc = VIDEO_STORM;
                    break;

                case "Snow":
                    backgroundVideoSrc = VIDEO_SNOW;
                    break;

                case "Smoke":
                    backgroundVideoSrc = VIDEO_FOGGY;
                    break;

                case "Haze":
                    backgroundVideoSrc = VIDEO_FOGGY;
                    break;

                case "Mist":
                    backgroundVideoSrc = VIDEO_FOGGY;
                    break;

                case "Fog":
                    backgroundVideoSrc = VIDEO_FOGGY;
                    break;

                case "Clouds":
                    backgroundVideoSrc = VIDEO_LIGHT_CLOUDY;
                    break;

                case "Sand":
                    backgroundVideoSrc = VIDEO_SAND;
                    break;
                    // Als er een beschrijving van het weer gegeven wordt wat nog niet bekend is
                default:
                    alert("Huidige beschrijving van het weer (" + weatherMain + ") is nog niet gedefineerd! \n Raadpleeg de ontwikkelaar om deze beschrijving erin te zetten.");
            }

            /* Initialiseer de weer-app */
            // Video
            let videoToPlay     = $("#backgroundVideoSrc").attr("src", "resources/videos/" + backgroundVideoSrc);
            let backgroundVideo = $("#backgroundMovie");
            backgroundVideo.append(videoToPlay);
            backgroundVideo[0].load();
            backgroundVideo[0].play();

            // Weersinformatie
            $("#weatherInfo").html(
                CITY_TO_QUERY + "<br>" +
                "<br><img src='" + TEMP_IMG     +"' width='25' title='Temperatuur'>&nbsp;"      + mainTemp + "℃" + "<br>" +
                "<img src='"     + HUMIDITY_IMG +"' width='25' title='Luchtvochtigheid'>&nbsp;" + mainHumidity + "%" + "<br>" +
                "<img src='"     + SUNRISE_IMG  +"' width='25' title='Zonsopgang'>&nbsp;"       + systemSunrise + "<br>" +
                "<img src='"     + SUNSET_IMG   +"' width='25' title='Zonsonderdag'>&nbsp;"     + systemSunset + "<br>&nbsp;"+
                "<img src='"     + WIND_IMG     +"' width='25' title='Wind snelheid'>&nbsp;"    + windSpeed + " km<br><br>" +
                "<img src='"     + WIND_DEG_IMG +"' width='35' title='Wind richting' style='transform:rotate("+windDeg+"deg)'><br>&nbsp;"+ windDegDesc +"<br><br>"
            ).fadeIn();

            /* als (response)status 404 of City not found */
        }).fail(function() {
            $("#weatherInfo").hide().html("Stad niet gevonden of geen reactie van API.<br>Probeer een andere stad uit of probeer het later nog eens!").fadeIn();
        });
    }

    /* Voer de functie uit bij het opstarten */
    getWeather();

    /* Update vervolgens om de x minuten (REFRESH_RATE) */
    setInterval(getWeather, REFRESH_RATE);

});