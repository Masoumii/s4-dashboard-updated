<?php
class Notification {
    
    /* Class Properties */
    private $notificationContent   = null;
    private $notificationType      = null;
    private $notificationTimestamp = null;
    private $notificationArray     = null;
  
    /* Class Constructor */
    public function __construct() {}
    
    /* Get content */
    public function getNotificationContent() {
        return $this->notificationContent;
    }
    
    /* Set content */
    public function setNotificationContent($content){
        $this->notificationContent = $content;
    }
    
    /* Get type */
    public function getNotificationType(){
        return $this->notificationType;
    }
    
    /* set type */
    public function setNotificationType($notificationType){
        $this->notificationType = $notificationType;
    }

    /* get Timestamp */
    public function getNotificationTimestamp(){
       return $this->notificationTimestamp; 
    }
    
    /* Set timestamp */
    public function setNotificationTimestamp($notificationTimestamp){
        $this->notificationTimestamp = $notificationTimestamp;
    }
    
    /* Get array */
    public function getNotificationArray(){
        return $this->notificationArray;
    }
    
    /* Mark As Read */
    public function readNotification(){
 
        /* Overzetten row naar notifications_archive tabel */
        require "dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $query = $conn->prepare("INSERT INTO notifications_archive(content,type) SELECT content,type FROM notifications");
        $query->execute();
        
        /* Verwijder record in originele tabel */
        $conn2 = DatabaseConnection::getConnection();
        $query2 = $conn2->prepare("DELETE FROM notifications");
        $query2->execute();

    }
    
    /* Toon notificatie */
    public function getNotification(){
        
        require("dbconfig.php");
        $conn = DatabaseConnection::getConnection();
        $query = $conn->prepare("SELECT * from notifications");
        $query->execute();

        while($row = $query->fetch()){
            
            /* Data ophalen uit DB om een notificatie te tonen */
            $this->setNotificationContent($row["content"]);
            $this->setNotificationTimestamp($row["timestamp"]);
            $this->setNotificationType($row['type']);

            /* Notificatie data naar array */
            $this->notificationArray = array(
                "content"    => $this->getNotificationContent(),
                "timestamp"  => date('H:i', strtotime($this->getNotificationTimestamp())),
                "type"       => $this->getNotificationType()
            );
        }
        /* Echo array ( JSON ) */
        echo json_encode($this->getNotificationArray());
    }
    
    /* Notificatie aanmaken */
    public function createNotification($content,$type){

        /* Set content en type */
        $this->setNotificationContent($content);
        $this->setNotificationType($type);
        
        /* Notificatie in DB tabel zetten */
        require "dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $query = $conn->prepare("INSERT INTO notifications(content,type) VALUES ('$this->notificationContent','$this->notificationType')");
        $query->execute();
    }  
}